function buttonClick() {
    var X1 = parseInt(document.getElementById("X1").value);
    var X2 = parseInt(document.getElementById("X2").value);
    var radio = document.getElementsByName("operation");
    if (Number.isNaN(X1) || Number.isNaN(X2))
        alert("In fields X1 and X2 must be only digits.");
    else if (document.getElementById("X1").value == "" || document.getElementById("X2").value == "")
        alert("Field must not be empty.")
    else if (X1 > X2)
        alert("X1 must be less then X2.");
    else {
        var resultDiv = document.getElementById("result");
            resultDiv.innerHTML = "";
            if(radio[0].checked)
                resultDiv.append("X1 + ... + X2 = " + sumBetween(X1, X2));
            else if(radio[1].checked)
                resultDiv.append("X1 * ... * X2 = " + multBetween(X1, X2));
            else
                resultDiv.append("Primes between X1 and X2: " + String(getPrimesArr(X1, X2)).split(',').join(", "));

    }
}

function clearFields() {
    document.getElementById("X1").value = "";
    document.getElementById("X2").value = "";
}

function sumBetween(X1, X2) {
    let result = 0;
    for (let i = X1; i <= X2; i++) {
        result += i;
    }
    return result;
}

function multBetween(X1, X2) {
    let result = 1;
    for (let i = X1; i <= X2; i++) {
        result *= i;
    }
    return result;
}

function getPrimesArr(X1, X2) {
    var sieve = [], i, j, primes = [];
    for (i = 2; i <= X2; ++i) {
        if (!sieve[i]) {
            if (i >= X1) {
                primes.push(i);
            }
            for (j = i << 1; j <= X2; j += i) {
                sieve[j] = true;
            }
        }
    }
    return primes;
}