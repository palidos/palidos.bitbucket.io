var tilesArray = ["3","3","4","4","5","5","6","6","7","7","8","8","9","9","10","10","J","J","Q","Q","K","K","A","A"];
var tilesValues = [];
var tilesIds = [];
var tilesFlipped = 0;

Array.prototype.shuffleTiles = function() {
    var i = this.length, j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
};

function newGame() {
    tilesFlipped = 0;
    var output = "";
    tilesArray.shuffleTiles();
    for (let i = 0; i < tilesArray.length; i++) {
        output += '<div class="card" id="tile' + i + '" onclick="flipTile(this,\'' + tilesArray[i] + "')\"></div>";
    }
    document.getElementById("memoryGame").innerHTML = output;
}

window.addEventListener("load", newGame);

function flipTile(tile, val) {
    if (tile.innerHTML == "" && tilesValues.length < 2) {
        tile.style.background = "url(images/" + val + ".png) no-repeat";
        tile.style.backgroundSize = "cover";

        if (tilesValues.length == 0) {
            tilesValues.push(val);
            tilesIds.push(tile.id);

        } else if (tilesValues.length == 1 && tilesIds[0] != tile.id) {
            tilesValues.push(val);
            tilesIds.push(tile.id);

            if (tilesValues[0] == tilesValues[1] && tilesIds[0] != tilesIds[1]) {
                tilesFlipped += 2;
                tilesValues = [];
                tilesIds = [];

                if (tilesFlipped == tilesArray.length) {
                    setTimeout(alert("Board cleared. Creating new board"), 3000);
                    document.getElementById("memoryGame").innerHTML = "";
                    newGame();
                }
            } else {
                function flipBack() {
                    var tile1 = document.getElementById(tilesIds[0]);
                    var tile2 = document.getElementById(tilesIds[1]);

                    tile1.style.background = "url(images/card_back.png) no-repeat";
                    tile1.style.backgroundSize = "cover";
                    tile2.style.background = "url(images/card_back.png) no-repeat";
                    tile2.style.backgroundSize = "cover";

                    tilesValues = [];
                    tilesIds = [];
                }
                setTimeout(flipBack, 1000);
            }
        }
    }
}
